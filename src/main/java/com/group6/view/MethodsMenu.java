package com.group6.view;

import com.group6.model.Team;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.group6.view.MyView.*;

public class MethodsMenu {

    private static Logger log = LogManager.getLogger(MethodsMenu.class);
    private Map<String, Printable> methodsMenu;
    private Map<String, String> actionsMenu;

    public MethodsMenu() {
        actionsMenu = new LinkedHashMap<>();
        actionsMenu.put("1", "1 - Sent to develop task");
        actionsMenu.put("2", "2 - Send task to review");
        actionsMenu.put("3", "3 - Test task");
        actionsMenu.put("4", "4 - Complete task");
        actionsMenu.put("5", "5 - Delete task");
        actionsMenu.put("Q", "Q - Back to task menu\n");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::sentToDevelop);
        methodsMenu.put("2", this::sendToReview);
        methodsMenu.put("3", this::testTask);
        methodsMenu.put("4", this::completeTask);
        methodsMenu.put("5", this::deleteTask);
    }

    private void sentToDevelop() {
        displayDevelopers();
        chooseDeveloper();
        currentTask.sentToDevelopTask(currentDeveloper);
    }

    private void sendToReview() {
        displayDevelopers();
        chooseDeveloper();
        currentTask.sendToReviewTask(currentDeveloper);
    }

    private void testTask() {
        displayDevelopers();
        chooseDeveloper();
        currentTask.testTask(currentDeveloper);
    }

    private void completeTask() {
        currentTask.completeTask();
    }

    private void deleteTask() {
        currentTask.deleteTask();
    }

    private void chooseDeveloper() {
        int developerNumber = Integer.parseInt(scanner.nextLine().trim());
        if ((developerNumber <= Team.getDevelopers().size()) && (developerNumber > 0)) {
            currentDeveloper = Team.getDevelopers().get(developerNumber - 1);
            log.info(currentDeveloper);
        } else {
            log.warn("Wrong input");
        }
    }

    private void displayDevelopers() {
        log.info("Please, choose developer's number");
        for (int i = 0; i < Team.getDevelopers().size(); i++) {
            log.info((i + 1) + ". " + Team.getDevelopers().get(i).toString());
        }
    }

    private void outputMenu1() {
        log.info("\nMENU:");
        for (String str : actionsMenu.values()) {
            log.info(str);
        }
    }

    public void showActions() {
        String keyMenu;
        do {
            outputMenu1();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
