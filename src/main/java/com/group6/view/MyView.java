package com.group6.view;

import com.group6.model.Board;
import com.group6.model.Developer;
import com.group6.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    protected static Board board = new Board();
    protected static Task currentTask;
    protected static Developer currentDeveloper;
    static Scanner scanner = new Scanner(System.in);
    private static Logger log = LogManager.getLogger(MyView.class);
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    public MyView() {
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menu.put("1", "1 - Add Task");
        menu.put("2", "2 - Choose task");
        menu.put("Q", "Q - Exit");

        menuMethods.put("1", this::addTask);
        menuMethods.put("2", this::chooseTask);
    }

    private void chooseTask() {
        log.info("Choose Task number:");
        int taskNumber = Integer.parseInt(scanner.nextLine().trim());
        if ((taskNumber <= board.getTaskList().size()) && (taskNumber > 0)) {
            currentTask = board.getTaskList().get(taskNumber - 1);
            log.info(currentTask);
            new MethodsMenu().showActions();
        } else {
            log.warn("There is no any task");
        }
    }

    private void addTask() {
        log.info("Enter task name:");
        Task task = new Task();
        task.setName(scanner.nextLine());
        board.addTask(task);
    }

    private void displayTable() {
        int count = 1;
        for (Task task : board.getTaskList()) {
            task.setId(count++);
            log.info(String.format("%4d%10s%10s%10s", task.getId(), task.getName(), task.getState(),
                    (task.getDeveloper() == null) ? "[       ]" : task.getDeveloper()));
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            log.trace(String.format("%4s%25s%25s%25s", "Id", "Task", "State", "Developer"));
            displayTable();
            outputMenu();
            System.out.print("Please, select menu point: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        scanner.close();
        System.exit(0);
    }
}
