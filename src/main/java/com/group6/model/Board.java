package com.group6.model;

import com.group6.model.Task;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private List<Task> taskList = new ArrayList<>();


    public Board() {
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void addTask(Task task) {
        taskList.add(task);
    }

}
