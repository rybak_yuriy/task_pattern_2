package com.group6.model;

public enum Position {

    TEAM_LEAD,
    DEVELOPER,
    QA_ENGINEER

}
