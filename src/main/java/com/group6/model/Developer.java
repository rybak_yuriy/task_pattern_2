package com.group6.model;

import java.util.ArrayList;
import java.util.List;

public class Developer {

    private String name;
    private Position position;
    private List<Task> tasks;

    public Developer(String name, Position position) {
        this.name = name;
        this.position = position;
        tasks = new ArrayList<>();
    }


    public Position getPosition() {
        return position;
    }

    public void addTask(Task task){
        tasks.add(task);
    }

    public void removeTask(Task task){
        tasks.remove(task);
    }

    @Override
    public String toString() {
        return "Developer{" +
                "name='" + name + '\'' +
                ", position=" + position +
                ", tasks=" + tasks +
                '}';
    }
}
