package com.group6.model;

import java.util.ArrayList;
import java.util.List;

public class Team {

    private static List<Developer> developers = new ArrayList<>();

    static {
        developers.add(new Developer("Dmytro Dmytrenko", Position.DEVELOPER));
        developers.add(new Developer("Petro Petrenko", Position.DEVELOPER));
        developers.add(new Developer("Stepan Stepanenko", Position.DEVELOPER));
        developers.add(new Developer("Taras Tarasenko", Position.DEVELOPER));
        developers.add(new Developer("Yuriy Yurchenko", Position.TEAM_LEAD));
        developers.add(new Developer("Ivan Ivanenko", Position.QA_ENGINEER));
        developers.add(new Developer("Mykola Mykolenko", Position.QA_ENGINEER));
    }

    public Team() {
    }

    public static List<Developer> getDevelopers() {
        return developers;
    }

}
