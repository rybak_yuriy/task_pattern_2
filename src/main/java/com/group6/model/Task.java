package com.group6.model;

import com.group6.model.state.State;
import com.group6.model.state.stateImpl.Todo;

public class Task {


    private int id;
    private String name;
    private State state;
    private Developer developer;

    public Task() {
        state = new Todo();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void sentToDevelopTask(Developer developer) {
        state.sentToDevelop(this, developer);
    }

    public void sendToReviewTask(Developer developer) {
        state.sentToReview(this, developer);
    }

    public void deleteTask() {
        state.delete(this);
    }

    public void testTask(Developer developer) {
        state.test(this, developer);
    }

    public void completeTask() {
        state.complete(this);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    @Override
    public String toString() {
        return "Task " + name;
    }
}
