package com.group6.model.state;

import com.group6.model.Developer;
import com.group6.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {

    Logger log = LogManager.getLogger(State.class);

    default void sentToDevelop(Task task, Developer developer) {
        log.info("Not allowed");
    }

    default void sentToReview(Task task, Developer developer) {
        log.info("Not allowed");
    }

    default void test(Task task, Developer developer) {
        log.info("Not allowed");
    }

    default void approve(Task task, Developer developer) {
        log.info("Not allowed");
    }

    default void complete(Task task) {
        log.info("Not allowed");
    }

    default void delete(Task task) {
        log.info("Not allowed");
    }

    default void setDeveloper(Developer developer) {
        log.info("Not allowed");
    }

    default void setTask(Task task) {
        log.info("Not allowed");
    }

}
