package com.group6.model.state.stateImpl;

import com.group6.model.Developer;
import com.group6.model.Position;
import com.group6.model.Task;
import com.group6.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Test implements State {

    private static Logger log = LogManager.getLogger(Test.class);
    private Task task;


    @Override
    public void sentToDevelop(Task task, Developer developer) {
        State state = new Development();
        state.setTask(task);
        if (developer != null) {
            state.setDeveloper(developer);
        }
        task.setState(state);
    }

    @Override
    public void complete(Task task) {
        task.getDeveloper().removeTask(task);
        task.setDeveloper(null);
        State state = new Complete();
        state.setTask(task);
        task.setState(state);
        log.info("Task is completed");
    }

    @Override
    public void setDeveloper(Developer developer) {
        if (developer.getPosition() == Position.QA_ENGINEER) {
            this.task.setDeveloper(developer);
        } else {
            log.warn("Position must be a QA_ENGINEER");
        }
    }

    @Override
    public void setTask(Task task) {
        this.task = task;
    }

    @Override
    public String toString() {
        return "Testing";
    }
}
