package com.group6.model.state.stateImpl;

import com.group6.model.Developer;
import com.group6.model.Task;
import com.group6.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Development implements State {
    private static Logger log = LogManager.getLogger(Development.class);
    private Task task;

    @Override
    public void sentToReview(Task task, Developer developer) {
        State state = new CodeReview();
        state.setTask(task);
        task.setDeveloper(developer);
        task.setState(state);
        log.info(String.format(" %s moved to code review state", task));
    }

    @Override
    public void delete(Task task) {
        State state = new Deleted();
        state.setTask(task);
        task.setState(state);
        task.setDeveloper(null);
        log.info(String.format(" %s moved to deleted state", task));
    }

    @Override
    public void setDeveloper(Developer developer) {
        task.setDeveloper(developer);
        log.info(String.format("Added developer %s to %s", developer, task));
    }

    @Override
    public void setTask(Task task) {
        this.task = task;
    }
}
