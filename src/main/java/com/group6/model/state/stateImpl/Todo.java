package com.group6.model.state.stateImpl;

import com.group6.model.Developer;
import com.group6.model.Position;
import com.group6.model.Task;
import com.group6.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Todo implements State {
    Logger log = LogManager.getLogger(Todo.class);

    private Task task;

    public Todo() {
    }

    @Override
    public void sentToDevelop(Task task, Developer developer) {
        if (developer.getPosition() != Position.QA_ENGINEER) {
            State state = new Development();
            state.setTask(task);
            state.setDeveloper(developer);
            task.setState(state);
            developer.addTask(task);
            log.info(String.format(" %s moved to development state", task));
        } else {
            log.warn("Position must be developer or team lead, but not a QA");
        }
    }

    @Override
    public void delete(Task task) {
        State state = new Deleted();
        state.setTask(task);
        task.setState(state);
        task.setDeveloper(null);
        log.info(String.format(" %s moved to deleted state", task));
    }

    @Override
    public void setDeveloper(Developer developer) {
        task.setDeveloper(developer);
        log.info(String.format("Added developer %s to %s", developer, task));
    }

    @Override
    public void setTask(Task task) {
        this.task = task;
    }

    @Override
    public String toString() {
        return "Todo";
    }
}
