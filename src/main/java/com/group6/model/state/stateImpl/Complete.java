package com.group6.model.state.stateImpl;

import com.group6.model.Developer;
import com.group6.model.Task;
import com.group6.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Complete implements State {

    private static Logger log = LogManager.getLogger(Complete.class);
    private Task task;

    @Override
    public void delete(Task task) {
        Deleted state = new Deleted();
        state.setTask(task);
        task.setState(state);
        task.setDeveloper(null);
        log.info(String.format(" %s moved to deleted state", task));
    }

    @Override
    public void setDeveloper(Developer developer) {
        this.task.setDeveloper(developer);
        log.info(String.format("Added %s to %s", developer, task));
    }

    @Override
    public String toString() {
        return "Done";
    }
}