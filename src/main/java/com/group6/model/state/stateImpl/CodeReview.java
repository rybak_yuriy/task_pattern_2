package com.group6.model.state.stateImpl;

import com.group6.model.Developer;
import com.group6.model.Position;
import com.group6.model.Task;
import com.group6.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CodeReview implements State {
    private static Logger log = LogManager.getLogger(CodeReview.class);
    Task task;

    @Override
    public void sentToDevelop(Task task, Developer developer) {
        State state = new Development();
        state.setTask(task);
        task.setState(state);
        log.info(String.format(" %s moved to development state", task));
    }

    @Override
    public void test(Task task, Developer developer) {
        if (developer.getPosition() == Position.QA_ENGINEER) {
            State state = new Test();
            state.setTask(task);
            task.setDeveloper(developer);
            task.setState(state);
            log.info(String.format("Moved %s to testing state", task));
        }else {
            log.warn("Position must be QA_ENGINEER");
        }
    }

    @Override
    public void delete(Task task) {
        State state = new Deleted();
        state.setTask(task);
        task.setState(state);
        task.setDeveloper(null);
        log.info(String.format(" %s moved to deleted state", task));
    }

    @Override
    public void setDeveloper(Developer developer) {
        task.setDeveloper(developer);
        log.info(String.format("Added developer %s to %s", developer, task));
    }

    @Override
    public void setTask(Task task) {
        this.task = task;
    }

    @Override
    public String toString() {
        return "CodeReview";
    }
}
