package com.group6.model.state.stateImpl;

import com.group6.model.Task;
import com.group6.model.state.State;

public class Deleted implements State {

    Task task;

    public Deleted() {
    }

    @Override
    public String toString() {
        return "Deleted{" +
                "task=" + task +
                '}';
    }
}
